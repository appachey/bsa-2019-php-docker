FROM php:7.2-cli

COPY src/Task1 ./src/Task1
COPY src/Task3 ./src/Task3
COPY vendor ./vendor
COPY tests ./tests
COPY phpunit.xml ./

CMD ["php", "-S", "0.0.0.0:8082", "-t", "/src/Task3"]
EXPOSE 8082